FROM gcc as builder
WORKDIR /app

COPY . .
RUN make -j2

FROM scratch

COPY --from=builder /app/app /app

CMD ["/app"]
