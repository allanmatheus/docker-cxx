# Docker example project for C++

This is a template for dockerizing C++ applications.  It should work for any
other language supported by GCC.

## Configuration

```
cp config.env .env
vi .env
```

Edit the `.env` file as needed.

## Only build inside a container

Useful for trying different versions of GCC.

Use the `build` script to call `make`.  You can edit this script to call
another build system, using something like `bash -c "autogen.sh && ./configure
&& make"` instead of just `make`.

## Build and run inside a container

Useful for distributing as an image and deploying as a container.

Use `build-production` to create the image and `run-production` to run the
application inside a new *ephemeral* container.

`build-production` can be customized editing the `Dockerfile` to handle other
build systems.
