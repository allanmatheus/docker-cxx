#include <iostream>

/**
 */
int main(int argc, char **argv)
{
    std::cout << "Hello, ";

    if (argc > 1) {
        for (int i = 1; i < argc; ++i) {
            std::cout << argv[i] << ' ';
        }
        std::cout << "\b!\n";

    } else {
        std::cout << "World!\n";
    }

    return 0;
}
