.SILENT:

CXX := g++
CXXFLAGS := -std=c++17 -pedantic
CPPFLAGS := -Wall -Wextra -static
LD := ${CXX}
LDLIBS :=
LDFLAGS := -static

PROG := app
SRCS := main.cc
OBJS := ${SRCS:.cc=.o}

${PROG}: ${OBJS}
	@echo "CXXLD $@"
	${LD} ${LDFLAGS} -o $@ $^ ${LDLIBS}

.cc.o:
	@echo "CXX $@"
	${CXX} ${CPPFLAGS} -c ${CXXFLAGS} -o $@ $^

clean:
	rm -f ${PROG} ${OBJS}

.PHONY: clean
